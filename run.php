<?php

// Requirements.
require 'vendor/autoload.php';
require 'settings.php';

// Be verbose.
error_reporting(E_ALL);

// S3 connection.
$s3 = new Aws\S3\S3Client([
    'region'  => AWS_REGION,
    'version' => 'latest',
    'credentials' => [
        'key'    => AWS_ACCESS_KEY_ID,
        'secret' => AWS_SECRET_ACCESS_KEY,
    ],
    'endpoint' => AWS_BUCKET_ENDPOINT,
]);

// Make timestamp.
date_default_timezone_set('UTC');
$timestamp = date('Y-m-d_H:i:s');

// File archive.
if (BACKUP_FILES) {
    $files_key = BACKUP_NAME . "_${timestamp}_files.tar.gz";
    $archive_files_path = BACKUP_TEMP . "/" . $files_key;
    $command = "tar -zcf $archive_files_path " . BACKUP_FILES_PATH;
    system($command, $result);
    if ($result !== 0) {
        die("File archive command failed! Aborting.");
    }

    $key = BACKUP_DESTINATION . "/" . BACKUP_NAME . "/" . $files_key;
    $s3->putObject([
        'Bucket' => AWS_BUCKET_NAME,
        'Key'    => $key,
        'SourceFile' => $archive_files_path,
    ]);

    unlink($archive_files_path);
}

// DB archive.
if (BACKUP_DB) {
    $db_key = BACKUP_NAME . "_${timestamp}_db.gz";
    $archive_db_path = BACKUP_TEMP . "/" . $db_key;
    $command = "mysqldump" .
        " --host=" . BACKUP_DB_HOST .
        " --port=" . BACKUP_DB_PORT .
        " --user=" . BACKUP_DB_USER .
        " --password='" . BACKUP_DB_PASS . "'" .
        " " . BACKUP_DB_NAME .
        " | gzip > $archive_db_path";
    system($command, $result);
    if ($result !== 0) {
        die("DB archive command failed! Aborting.");
    }

    $key = BACKUP_DESTINATION . "/" . BACKUP_NAME . "/" . $db_key;
    $s3->putObject([
        'Bucket' => AWS_BUCKET_NAME,
        'Key'    => $key,
        'SourceFile' => $archive_db_path,
    ]);
    unlink($archive_db_path);
}
