<?php

/**
 * Backup identifier.
 * All backup files are stored in S3 under a folder with this name.
 */
define("BACKUP_NAME", "test.com");
define("BACKUP_DESTINATION", "last-3-months");
define("BACKUP_TEMP", "/tmp");

/**
 * S3 connection.
 */
define("AWS_REGION", "pl-waw");
define("AWS_ACCESS_KEY_ID", "");
define("AWS_SECRET_ACCESS_KEY", "");
define("AWS_BUCKET_NAME", "backup-xyz");
define("AWS_BUCKET_ENDPOINT", "https://s3.pl-waw.scw.cloud/backup-xyz");

/**
 * Source files to backup and temporary folder for building archives.
 */
define("BACKUP_FILES", true);
define("BACKUP_FILES_PATH", ".");

/**
 * Source MySQL database to backup.
 */
define("BACKUP_DB", true);
define("BACKUP_DB_HOST", "localhost");
define("BACKUP_DB_PORT", 3306);
define("BACKUP_DB_NAME", "...");
define("BACKUP_DB_USER", "...");
define("BACKUP_DB_PASS", "...");
